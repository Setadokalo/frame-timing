use std::time::{Duration, Instant};

struct FramerateCalculator {
	data: Vec<f64>,
	current_frame: usize,
}

impl FramerateCalculator {
	pub fn new(frames_to_sample: usize) -> FramerateCalculator {
		let mut data = Vec::with_capacity(frames_to_sample);
		for _ in 0..frames_to_sample {
			data.push(0.0);
		}
		FramerateCalculator {
			data,
			current_frame: 0,
		}
	}
	pub fn step(&mut self, delta: f64) {
		if let Some(pointer) = self.data.get_mut(self.current_frame) {
			*pointer = delta;
		} else {
			println!("error - object not initialized properly")
		}
		self.current_frame += 1;
		if self.current_frame >= self.data.len() {
			self.current_frame = 0;
		}
	}
	pub fn calculate_time_per_frame(&self) -> f64 {
		let mut average = 0.0;
		for &time in self.data.iter() {
			average += time;
		}
		average / (self.data.len() as f64)
	}
	pub fn calculate_framerate(&self) -> f64 {
		1.0 / self.calculate_time_per_frame()
	}
}

pub struct Timing {
	target_frame_time: f64,
	framerate_calc: FramerateCalculator,
	start_instant: Instant,
	frame_start_instant: Instant,

	last_print_time: f64,

	delta: f64,
	is_print_tick: bool,
	print_framerate: bool,
}
impl Timing {
	pub fn new(sample_count: usize, framerate_target: f64, print_framerate: bool) -> Self {
		Timing {
			target_frame_time: 1.0 / framerate_target,
			framerate_calc: FramerateCalculator::new(sample_count),
			start_instant: Instant::now(),
			frame_start_instant: Instant::now(),
			last_print_time: 0.0,
			delta: 0.0,
			is_print_tick: false,
			print_framerate,
		}
	}

	#[allow(dead_code)]
	pub fn is_print_tick(&self) -> bool {
		self.is_print_tick
	}

	pub fn step_timing(&mut self) -> f64 {
		// to decrease desync we create the new frame start time *immediately* on stepping the timing, then do all other logic, then save the new start instant
		let temp = Instant::now();
		self.is_print_tick = false;

		self.delta = self.frame_start_instant.elapsed().as_secs_f64();
		self.framerate_calc.step(self.delta);
		let time_passed = self.start_instant.elapsed().as_secs_f64();

		if time_passed > self.last_print_time + 3.0 {
			self.is_print_tick = true;
			if self.print_framerate {
				// frame_counter *should* handle this but for some reason it thinks my framerate is infinite soooo
				// we're using a sampler directly
				println!(
					"{:.3} FPS | Total time elapsed {:.3}s",
					self.framerate_calc.calculate_framerate(),
					time_passed
				);
				self.last_print_time = time_passed;
				// frame_count = 0;
			}
		}
		// frame_count += 1;
		self.frame_start_instant = temp;
		self.delta
	}

	pub fn get_remaining_time(&self) -> f64 {
		self.target_frame_time - self.frame_start_instant.elapsed().as_secs_f64()
	}

	pub fn sleep_remaining(&mut self) {
		let time_to_sleep = self.get_remaining_time();
		if time_to_sleep > 0.0 {
			spin_sleep::sleep(Duration::from_secs_f64(time_to_sleep));
		}
	}
}
